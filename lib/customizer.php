<?php
/**
 * BPRESS Customizer Settings
 * Add/Remove/Change settings to WordPress Theme
 *
 * @package NARGA BPress
 * @since 1.0
 * @author Nguyễn Đình Quân (dinhquan@narga.net / http://www.narga.net/)
 * @copyright Copyright (c) 2019, Nguyen Dinh Quan a.k.a narga
 *
 */

/**
 * NARGA Customizer Settings
 *
 * @since NARGA BPRESS v0.5
 **/

add_action( 'customize_register', 'bpress_customizer' );
function bpress_customizer($wp_customize){

    $wp_customize = array(
     array(
     'id' => 'header_scripts',
     'label' => 'Header Scripts',
     'type' => 'textarea',
     'default' => ''
     ),
    array(
      'id' => 'footer_scripts',
     'label' => 'Footer Scripts',
     'type' => 'textarea',
     'default' => ''
   ),
    // ...

    );

    beans_register_wp_customize_options( $wp_customize, 'bpress_addition_scripts', array(
     'title' => 'Additional Scripts' ) );

}

?>
