<?php
/**
 * The singular template file.
 *
 * @package WordPress
 * @subpackage NARGA BPress
 * @since NARGA BPress 1.0
 *
 */

/* Author Bio Box */
add_action( 'beans_post_after_markup', 'bpress_author_box' );
function bpress_author_box() {

    echo '<div id="author-info" class="uk-panel uk-panel-box" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
      <div class="uk-clearfix">';
    echo '<h3>' . get_the_author_meta( 'display_name' ) . '</h3>';
    echo get_avatar( get_the_author_meta( 'user_email' ), '80', '', 'The author avatar', array( 'class' => array( 'uk-border-circle', 'uk-align-medium-right' ) ) );
    echo '<div itemprop="description">' . get_the_author_meta('description') . '</div>';
    echo '</div>
</div>';

}

// Load beans document
beans_load_document();
