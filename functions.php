<?php
/**
 * BPress functions and definitions
 *
 * @link https://www.narga.net/bpress
 *
 * @package WordPress
 * @subpackage NARGA BPress - Child Theme for Beans Framework
 * @since 1.0.0
 *
 **/

// Include Beans
require_once( get_template_directory() . '/lib/init.php' );

// Include Customizer lib
require_once( get_stylesheet_directory().'/lib/customizer.php' );

// Enqueue uikit assets
beans_add_smart_action( 'beans_uikit_enqueue_scripts', 'bpress_enqueue_uikit_assets', 5 );

function bpress_enqueue_uikit_assets() {

    // Enqueue uikit overwrite theme folder
    beans_uikit_enqueue_theme( 'bpress', get_stylesheet_directory_uri() . '/assets/less/uikit' );

    // Add the theme style as a uikit fragment to have access to all the variables
    beans_compiler_add_fragment( 'uikit', get_stylesheet_directory_uri() . '/assets/less/style.less', 'less' );

    // Include the uikit components needed
    beans_uikit_enqueue_components( array( 'sticky' ), 'add-ons' );

}

// Enqueue Google Fonts as the WordPress way
add_action( 'wp_enqueue_scripts', 'embed_google_fonts' );

function embed_google_fonts() {
    wp_enqueue_style( 'google_fonts', 'https://fonts.googleapis.com/css?family=Oswald:400|Quattrocento:400,700' );
}

// Remove JQuery migrate
function bpress_remove_jquery_migrate($scripts)
{
    if (!is_admin() && isset($scripts->registered['jquery'])) {
        $script = $scripts->registered['jquery'];

        if ($script->deps) { // Check whether the script has any dependencies
            $script->deps = array_diff($script->deps, array(
                'jquery-migrate'
            ));
        }
    }
}

add_action('wp_default_scripts', 'bpress_remove_jquery_migrate');

/**
 * Disable the emoji's
 */
function disable_emojis() {
 remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
 remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
 remove_action( 'wp_print_styles', 'print_emoji_styles' );
 remove_action( 'admin_print_styles', 'print_emoji_styles' );
 remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
 remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
 remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
 add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
 add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param array $plugins
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
 if ( is_array( $plugins ) ) {
 return array_diff( $plugins, array( 'wpemoji' ) );
 } else {
 return array();
 }
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
 if ( 'dns-prefetch' == $relation_type ) {
 /** This filter is documented in wp-includes/formatting.php */
 $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

$urls = array_diff( $urls, array( $emoji_svg_url ) );
 }

return $urls;
}

/**
 *  Header Adjustment
 **/

// Scripts, styles or meta tags added in the <head>.
add_action( 'wp_head', 'bpress_header_scripts' );

function bpress_header_scripts() {
    echo get_theme_mod( 'header_scripts' );
}

// Remove header block padding to decrease the size of tm-header, set primary
// backgroud color, transform all text in header section to uppercase
beans_add_attribute( 'beans_header', 'class', 'uk-padding-remove uk-block-primary uk-text-uppercase' );

// Remove the site title tag.
beans_remove_action( 'beans_site_title_tag' );

//Enable a sticky header
add_action( 'beans_before_load_document', 'add_class_sticky_beans_header' );

function add_class_sticky_beans_header() {
    beans_add_attribute( 'beans_header', 'data-uk-sticky', 'top:0' );
}

// Display posts in a responsive dynamic grid.
add_action( 'wp', 'bpress_posts_grid' );

function bpress_posts_grid() {

    // Only apply to non singular view.
    if ( !is_singular() ) {

    // Add grid.
    beans_wrap_inner_markup( 'beans_content', 'bpress_posts_grid', 'div',
        array(  'class'                => 'uk-grid uk-grid-match',
                'data-uk-grid-margin'  => '' ) );

    if ( !is_paged() )
        beans_wrap_markup( 'beans_post', 'bpress_posts_grid_column', 'div' );
    else
        beans_wrap_markup( 'beans_post', 'bpress_posts_grid_column', 'div',
         array( 'class' => 'uk-width-large-1-2 uk-width-medium-1-2' ) );

    // Move the posts pagination after the new grid markup.
    beans_modify_action_hook( 'beans_posts_pagination', 'bpress_posts_grid_after_markup' );

  }
}

add_filter( 'bpress_posts_grid_column_attributes', 'bpress_posts_grid_column_attributes' );

/**
 * Add post grid columns adaptive class.
 *
 * @param array $attributes Array of post grid column HTML attributes.
 *
 * @return array Array of post grid column HTML attributes.
 *
 * From https://community.getbeans.io/discussion/creating-a-varied-grid-on-home-page/
 *
 * $columns = ( 0 === $index % 5 ) ? '2-3' : '1-3';
 * All the logic to apply the correct width on the post according to your needs
 * (eg. if the post format is "aside") should be done in the
 * example_post_grid_column_attributes() function above. In the snippet posted
 * in my as reply, you see that the first and every multiple of 6th post are
 * set to take 2 columns here ->
 * $columns = ( 0 === $index % 5 ) ? '2-3' : '1-3'; which is where you may add
 * additional logic to fit your needs.
 *
 */

function bpress_posts_grid_column_attributes( $attributes ) {
   static $index = 0;

   // Add space after current class if it exists.
   $attributes['class'] = isset( $attributes['class'] ) ? $attributes['class'] . ' ' : null;

   // Add adaptive grid class.
   $columns = ( 0 === $index ) ? '1-1' : '1-2';
   $attributes['class'] .= "uk-width-large-{$columns} uk-width-medium-1-2";

   // Bump post index.
   $index++;

   return $attributes;

}

// Add CSS class to Continue reading
beans_add_attribute( 'beans_post_more_link', 'class', 'uk-button uk-button-success uk-margin-top' );

// Setup BPRESS
beans_add_smart_action( 'beans_before_load_document', 'bpress_index_setup_document' );

function bpress_index_setup_document() {

    // Move featured image above title
    beans_modify_action( 'beans_post_image', 'beans_post_header_before_markup', 'beans_post_image' );

    // Remove featured image in singular view
    if ( is_singular() ) {

        beans_remove_action( 'beans_post_image' );

        // Display modified date instead published date
        add_filter( 'beans_post_meta_date_text_output', 'bpress_last_updated' );

    } else {

        // Only display post tags on singular content
        beans_remove_action( 'beans_post_meta_tags' );

    }

    // Only display the total number of comments
    add_filter( 'beans_post_meta_item_comments_text_output', 'bpress_total_comments' );

    function bpress_total_comments() {
        return get_comments_number();
    }

    // Display modified date instead published date function
    function bpress_last_updated() {
        return 'Last Updated on ' . get_the_modified_date();
    }

    // Complete remove the post meta categories.
    beans_remove_action( 'beans_post_meta_categories' );

    // Re-arrange post meta info
    add_filter( 'beans_post_meta_items', 'bpress_sort_meta_items' );

    function bpress_sort_meta_items( $meta_items ) {

        $meta_items = array (
            'categories'   => 10, // priority
            'date'     => 20, // priority
            'comments' => 30  // priority
        );

        return $meta_items;

    }

    // Set post meta text appear as uppercase
    beans_add_attribute( 'beans_post_meta', 'class', 'uk-text-uppercase' );
    beans_add_attribute( 'beans_post_meta_item[_comments]', 'class', 'uk-text-uppercase' );

    // Remove meta date text color
    beans_add_attribute( 'beans_post_meta_date', 'class', 'uk-text-muted' );

    // Replace Posted On text with clock icon
    add_filter( 'beans_post_meta_date_prefix_output', 'modify_posted_on' );

    function modify_posted_on() {
        return '<i class="uk-icon-calendar uk-text-muted uk-margin-small-right"></i>';
    }

    // Replace Filed Under text with folder icon
    add_filter( 'beans_post_meta_categories_prefix_output', 'modify_filed_under' );

    function modify_filed_under() {
        return '<i class="uk-icon-folder uk-text-muted uk-margin-small-right"></i>';
    }

    // Add icon comments meta
    add_filter( 'beans_post_meta_item[_comments]_prepend_markup', 'bpress_comment_icon' );

    function bpress_comment_icon() {
        echo '<i class="uk-icon-comments uk-text-muted uk-margin-small-right"></i>';
    }

   // Add icon to tag list (sigular view)
    add_filter( 'beans_post_meta_tags_prefix_output', 'modify_tagged_with' );

    function modify_tagged_with() {
        return '<i class="uk-icon-tags uk-text-muted uk-margin-small-right"></i>';
    }

    // Edit post/page lini
    add_action( 'beans_post_body_append_markup', 'bpress_edit_post' );

    function bpress_edit_post() {
        echo '<p class="uk-margin-bottom-remove">' . edit_post_link() . '</p>';
    }
}

/**
 *  Footer Adjustment
 **/

// Scripts added at the bottom of the page.
add_action( 'wp_footer', 'bpress_footer_scripts' );

function bpress_footer_scripts() {
    echo get_theme_mod( 'footer_scripts' );
}

//* Add Bottom widgets.
add_action( 'widgets_init', 'bpress_widget_area' );

function bpress_widget_area() {

    beans_register_widget_area( array(
        'name' => 'Bottom Area',
        'id' => 'bottom-area',
        'description' => 'Widgets in this area will be shown in the bottom section as a grid.',
        'beans_type' => 'grid'
        ) );

}

add_action( 'beans_footer_before_markup', 'bpress_bottom_widget_area' );

function bpress_bottom_widget_area() {

    // Stop here if no widget
    if( !beans_is_active_widget_area( 'bottom-area' ) )
        return;

    echo '<div class="tm-pre-footer uk-block">
        <div class="uk-container uk-container-center">'
        . beans_get_widget_area_output( 'bottom-area' ) .
        '</div>
      </div>';

}

// Register new Footer Menu
function register_footer_menu() {
    register_nav_menu( 'footer-menu',__( 'Footer Menu' ) );
}

add_action( 'init', 'register_footer_menu' );

// Add the new secondary menu - where it is to be located
beans_add_smart_action( 'beans_fixed_wrap[_footer]_before_markup', 'bpress_footer_menu' );

// Overwrite the footer credit and add the footer menu
// beans_add_smart_action( 'beans_footer_credit_right_text_output', 'footer_menu' );


function bpress_footer_menu() {
    wp_nav_menu( array(
        'menu' => 'Footer Menu',
        'menu_class' => 'uk-subnav',
        'container'       => 'div',
        'container_class' => 'tm-secondary-nav uk-container uk-container-center',
        'theme_location' => 'footer-menu',
        'beans_type' => 'navbar'
    ) );
}

// Replace right footer text with own information
add_action( 'beans_footer_credit_right_text_output', 'bpress_right_copyright' );

function bpress_right_copyright() {
    echo 'Proudly powered by <a href="https://www.narga.net/go/digitalocean/" title="Digital Ocean Cloud Hosting">Digital Ocean</a> · <a href="https://www.getbeans.io/">Beans</a> · <a href="https://wordpress.org/">WordPress</a></div>';

}

// Comments section adjustment

// Change name and email inputs as equal by modified CSS grid
beans_replace_attribute( 'beans_comment_form[_name]', 'class', 'uk-width-medium-1-3', 'uk-width-medium-1-2');
beans_replace_attribute( 'beans_comment_form[_email]', 'class', 'uk-width-medium-1-3', 'uk-width-medium-1-2');

// Hide comment form Name, Email and Website legends
beans_add_attribute( 'beans_comment_form_legend[_name]', 'class', 'uk-hidden');
beans_add_attribute( 'beans_comment_form_legend[_email]', 'class', 'uk-hidden');
beans_add_attribute( 'beans_comment_form_legend', 'class', 'uk-hidden');

// Add comment form Name and Email into input as placeholder
beans_add_attribute( 'beans_comment_form_field[_name]', 'placeholder', 'Name *');
beans_add_attribute( 'beans_comment_form_field[_email]', 'placeholder', 'Email *');

// Remove Comment URL Input
beans_remove_markup( 'beans_comment_form[_website]' );

// Remove Comment URL input
beans_remove_markup( 'beans_comment_form_field[_url]' );

// Remove comment form URL legend.
beans_remove_action( 'beans_comment_form_legend_text[_url]_output' );

?>
